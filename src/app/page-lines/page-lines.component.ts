import { Component, ElementRef, OnInit } from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-page-lines',
  templateUrl: './page-lines.component.html',
  styleUrls: ['./page-lines.component.scss'],
})
export class PageLinesComponent implements OnInit {
  constructor(private readonly el: ElementRef) {}

  ngOnInit(): void {
    const element: Element = this.el.nativeElement.querySelector('section');
    const container = d3
      .select(element)
      .append('svg')
      .attr('width', element.clientWidth)
      .attr('height', element.clientHeight);

    const limit = 53.35;
    const widthCount = Math.ceil(element.clientWidth / limit);
    const heightCount = Math.ceil(element.clientHeight / limit);
    const animationMS = 300;

    // horizontal lines
    for (let i = 1; i < heightCount; i++) {
      container
        .append('line')
        .attr('x1', 0)
        .attr('y1', i * limit)
        .attr('x2', element.clientWidth)
        .attr('y2', i * limit)
        .attr('stroke-width', 1)
        .attr('stroke', 'black');
    }

    // vertical lines
    for (let i = 1; i < widthCount; i++) {
      container
        .append('line')
        .attr('x1', i * limit)
        .attr('y1', 0)
        .attr('x2', i * limit)
        .attr('y2', element.clientHeight)
        .attr('stroke-width', 1)
        .attr('stroke', 'black');
    }

    // dots
    for (let i = 0; i < widthCount; i++) {
      for (let j = 0; j < heightCount; j++) {
        setTimeout(() => {
          container
            .append('circle')
            .attr('cx', i * limit + limit / 2)
            .attr('cy', j * limit + limit / 2)
            .transition()
            .duration(animationMS)
            .ease(d3.easeLinear)
            .attr('r', Math.floor(Math.random() * (limit / 5)) + limit / 10)
            .attr('fill', this.getRandomHexColor());
        }, j * heightCount);
      }
    }

    // for (let i = 0; i < 100; i++) {
    //   setTimeout(() => {
    //     container
    //       .append('circle')
    //       .attr('cx', Math.floor(Math.random() * element.clientWidth))
    //       .attr('cy', Math.floor(Math.random() * element.clientHeight))
    //       .transition()
    //       .duration(animationMS)
    //       .ease(d3.easeLinear)
    //       .attr('r', Math.floor(Math.random() * (limit / 5)) + limit / 5)
    //       .attr('fill', this.getRandomHexColor());
    //   }, (i * animationMS) / 10);
    // }
  }

  getRandomHexColor(): string {
    const r = Math.floor(Math.random() * 256);
    const g = Math.floor(Math.random() * 256);
    const b = Math.floor(Math.random() * 256);

    const rHex = this.decimalToHex(r);
    const gHex = this.decimalToHex(g);
    const bHex = this.decimalToHex(b);

    return `#${rHex}${gHex}${bHex}`;
  }

  decimalToHex(decimal: number): string {
    return (decimal + Math.pow(16, 6)).toString(16).substr(5);
  }
}
